/*
 * util.h
 *
 * Created: 07.06.2014 18:30:05
 *  Author: Sven Dummis
 */ 

#pragma once
#include "NixieUhr.h"

#define bit_set(p,m) ((p) |= (m))
#define bit_clear(p,m) ((p) &= ~(m))
#define bit_get(p,m) ((p) & (m))
#define BIT(x) (0x01 << (x))
#define bit_flip(p,m) ((p) ^= (m))

#define HIGHNIBBLE(b) ((b >> 4) & 0x0F)
#define LOWNIBBLE(b) ((b) & 0x0F)

#define COMBINENIBBLE(h,l) ((h << 4) | l)

typedef struct color_rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} color_rgb_t;

typedef struct color_hsv {
	uint8_t h;
	uint8_t s;
	uint8_t v;
} color_hsv_t;

uint8_t bcd2bin (uint8_t val);

uint8_t bin2bcd (uint8_t val);

uint8_t checkBoundariesAndAdjust(enum KeyAction Action, uint8_t* val, uint8_t upperBound, uint8_t lowerBound);

bool isDisplayOn(uint8_t *hour, uint8_t *minute);

bool isSummertime(void);

int DS1307_readSRAMConfig_t(void);

int DS1307_writeSRAMConfig_t(void);

color_rgb_t colorWheel(uint8_t wheelPos);

void fx_random(char *buf, uint16_t size);

int myRand();

//color_rgb_t hsv_to_rgb (color_hsv_t hsv);
