/*
 * clock.c
 *
 * Created: 24.07.2014 19:28:51
 *  Author: Sven Dummis
 */ 

#include <avr/interrupt.h>
#include "../NixieUhr.h"
#include "../DS1307.h"
#include "../util.h"
#include "date.h"


uint8_t cnt;
uint8_t maxCnt;

bool bWokenUp;

void Date_InitHandlers(void)
{
	EventHandler[HANDLER_MODE] = Date_KeyMode;
	EventHandler[HANDLER_MODE_LONG] = Date_KeyModeLong;
	EventHandler[HANDLER_DOWORK] = Date_DoWork;
	EventHandler[HANDLER_500ms_TICK] = Date_On500msTick;
	cnt = 0;
	maxCnt = 20;
}

void Date_On500msTick(void)
{
	if (cnt < maxCnt)
	{
		if (bDisplayOn == false)
		{
			bWokenUp = true;
			bDisplayForcedOn = true;
		}
		cnt++;
	}
	else
	{
		if (bWokenUp == true)
		{
			bDisplayForcedOn = false;
			bWokenUp = false;
		}
		currMode = Clock;
		Date_DeInitHandlers();
	}
}

void Date_DoWork(void)
{
	cli();	
	shiftdata[SHIFT_DATA_YEAR] = bin2bcd((uint8_t)(date.year - 2000));
	sei();
	shiftdata[SHIFT_DATA_MONTH] = bin2bcd(date.month);
	shiftdata[SHIFT_DATA_DAY] = bin2bcd(date.dayOfMonth);
	/*if (date.month == 10 && date.dayOfMonth == 4)
	{
		marriage = 1;
	}
	else
		marriage = 0;*/

	//ds1307_gettime_as_bcd(&shiftdata[SHIFT_DATA_HOURS], &shiftdata[SHIFT_DATA_MINUTES], &shiftdata[SHIFT_DATA_SECONDS]);
	shiftdata[SHIFT_DATA_DP] = 0; // f�r die dezimalpunkte
}

void Date_KeyMode(void)
{
	checkBoundariesAndAdjust(Plus,&currMode,MAX_MODE,0);
	Date_DeInitHandlers();
}

void Date_KeyModeLong(void)
{
	currMode = Setup;
	Date_DeInitHandlers();
}

void Date_DeInitHandlers(void)
{
	SetDefaultHandlers();
	ModeInitHandlers[currMode]();
}