/*
 * clock.h
 *
 * Created: 24.07.2014 19:29:32
 *  Author: Sven Dummis
 */ 

#pragma once

void Clock_InitHandlers(void);
void Clock_DeInitHandlers(void);
void Clock_KeyMode(void);
void Clock_KeyModeLong(void);
void Clock_DoWork(void);
void Clock_KeyPlus(void);
void Clock_On500msTick(void);
void Clock_KeyMinus(void);
void Clock_KeyOk(void);
