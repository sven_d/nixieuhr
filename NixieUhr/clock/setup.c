/*
 * setup.c
 *
 * Created: 23.07.2014 17:07:43
 *  Author: Sven Dummis
 */ 

#ifndef NIXIEUHR_H_
#include "../NixieUhr.h"
#endif
#include "../ds1307.h"
#include "../util.h"
#include "setup.h"
#include "../util.h"
#include <util/atomic.h>
#include <avr/interrupt.h>

bool bBlinkOn;
bool bSkipAutoIncrement;
SRAMConfig_t oldConfig;
enum Setup_SubMode subMode;

void Blink(uint8_t *what, uint8_t *val)
{
	if (bBlinkOn == false)
	{
		*what = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
	}
	else
	{
		*what = bin2bcd(*val);
	}
}

void HandleModeSetup(enum KeyAction Action)
{	
	if (subMode == Day || subMode == Month || subMode == Year)
	{
		shiftdata[SHIFT_DATA_DAY] = bin2bcd(configdata[CONFIG_DATA_DAY]);
		shiftdata[SHIFT_DATA_MONTH] = bin2bcd(configdata[CONFIG_DATA_MONTH]);
		shiftdata[SHIFT_DATA_YEAR] = bin2bcd(configdata[CONFIG_DATA_YEAR]);
	}
	else if (subMode == Hour || subMode == Minute || subMode == Second)
	{
		shiftdata[SHIFT_DATA_HOURS] = bin2bcd(configdata[CONFIG_DATA_HOURS]);
		shiftdata[SHIFT_DATA_MINUTES] = bin2bcd(configdata[CONFIG_DATA_MINUTES]);
		shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(configdata[CONFIG_DATA_SECONDS]);
	}
	
	switch (subMode)
	{
		case Init:
			cli();	
			configdata[CONFIG_DATA_YEAR] = (uint8_t)(date.year - 2000);
			sei();
			configdata[CONFIG_DATA_MONTH] = date.month;
			configdata[CONFIG_DATA_DAY] = date.dayOfMonth;
			configdata[CONFIG_DATA_HOURS] = date.hours;
			configdata[CONFIG_DATA_MINUTES] = date.minutes;
			configdata[CONFIG_DATA_SECONDS] = date.seconds;
			oldConfig = sram_config;
			subMode = Day;
			break;
		
		case Day:
			checkBoundariesAndAdjust(Action,&configdata[CONFIG_DATA_DAY],31,1);
			if (bBlinkOn == false)
			{
				shiftdata[SHIFT_DATA_DAY] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			}
			break;
		
		case Month:
			checkBoundariesAndAdjust(Action,&configdata[CONFIG_DATA_MONTH],12,1);
			if (bBlinkOn == false)
			{
				shiftdata[SHIFT_DATA_MONTH] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			}
			break;
		
		case Year:
			checkBoundariesAndAdjust(Action,&configdata[CONFIG_DATA_YEAR],99,0);
			if (bBlinkOn == false)
			{
				shiftdata[SHIFT_DATA_YEAR] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			}
			break;
		
		case Hour:
			checkBoundariesAndAdjust(Action,&configdata[CONFIG_DATA_HOURS],23,0);
			if (bBlinkOn == false)
			{
				shiftdata[SHIFT_DATA_HOURS] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			}
			break;
		
		case Minute:
			checkBoundariesAndAdjust(Action,&configdata[CONFIG_DATA_MINUTES],59,0);
			if (bBlinkOn == false)
			{
				shiftdata[SHIFT_DATA_MINUTES] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			}
			break;
		
		case Second:
			checkBoundariesAndAdjust(Action,&configdata[CONFIG_DATA_SECONDS],59,0);
			bSkipAutoIncrement = true;
			if (bBlinkOn == false)
			{
				shiftdata[SHIFT_DATA_SECONDS] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			}
			break;
			
		case SwitchOnHour:
			checkBoundariesAndAdjust(Action, &sram_config.configOnHour,23,0);
			shiftdata[SHIFT_DATA_HOURS] = 1;
			shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(sram_config.configOnMinute);
			Blink(&shiftdata[SHIFT_DATA_MINUTES],&sram_config.configOnHour);
			break;
			
		case SwitchOnMinute:
			checkBoundariesAndAdjust(Action, &sram_config.configOnMinute,59,0);
			shiftdata[SHIFT_DATA_HOURS] = 1;
			shiftdata[SHIFT_DATA_MINUTES] = bin2bcd(sram_config.configOnHour);
			Blink(&shiftdata[SHIFT_DATA_SECONDS],&sram_config.configOnMinute);
			break;
			
		case SwitchOffHour:
			checkBoundariesAndAdjust(Action, &sram_config.configOffHour,23,0);
			shiftdata[SHIFT_DATA_HOURS] = 0;
			shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(sram_config.configOffMinute);
			Blink(&shiftdata[SHIFT_DATA_MINUTES],&sram_config.configOffHour);
			break;
		
		case SwitchOffMinute:
			checkBoundariesAndAdjust(Action, &sram_config.configOffMinute,59,0);
			shiftdata[SHIFT_DATA_HOURS] = 0;
			shiftdata[SHIFT_DATA_MINUTES] = bin2bcd(sram_config.configOffHour);
			Blink(&shiftdata[SHIFT_DATA_SECONDS],&sram_config.configOffMinute);
			break;
			
		case ColorMode:
			checkBoundariesAndAdjust(Action,&sram_config.configColorMode, 1, 0);
			shiftdata[SHIFT_DATA_HOURS] = 2;
			shiftdata[SHIFT_DATA_MINUTES] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			shiftdata[SHIFT_DATA_SECONDS] = COMBINENIBBLE(ILLEGAL_BCD,LOWNIBBLE(bin2bcd(sram_config.configColorMode)));
			sram_config.colorIndex = 0; //colorIndex == WheelPosition	
			break;
			
		case ColorTime:
			checkBoundariesAndAdjust(Action, &sram_config.configColorTime, 255, 1);
			shiftdata[SHIFT_DATA_HOURS] = 3;
			shiftdata[SHIFT_DATA_MINUTES] = COMBINENIBBLE(ILLEGAL_BCD,LOWNIBBLE(bin2bcd(sram_config.configColorTime / 100)));
			shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(sram_config.configColorTime % 100);
			break;
		
		case Color:
			checkBoundariesAndAdjust(Action,&sram_config.colorIndex,255,0);
			shiftdata[SHIFT_DATA_HOURS] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			shiftdata[SHIFT_DATA_MINUTES] = COMBINENIBBLE(ILLEGAL_BCD,LOWNIBBLE(bin2bcd(sram_config.colorIndex / 100)));
			shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(sram_config.colorIndex % 100);
			color_rgb_t color_out;
			color_out = colorWheel(sram_config.colorIndex);
			sram_config.colorR = color_out.r;
			sram_config.colorG = color_out.g;
			sram_config.colorB = color_out.b;
			break;
			
		case SlotMachine:
			checkBoundariesAndAdjust(Action,&sram_config.configSlotMachine, 1, 0);
			shiftdata[SHIFT_DATA_HOURS] = 4;
			shiftdata[SHIFT_DATA_MINUTES] = COMBINENIBBLE(ILLEGAL_BCD,ILLEGAL_BCD);
			shiftdata[SHIFT_DATA_SECONDS] = COMBINENIBBLE(ILLEGAL_BCD,LOWNIBBLE(bin2bcd(sram_config.configSlotMachine)));	
	}
	
}

// Werte �bernehmen, Uhr stellen, SRAM schreiben
void Setup_KeyOKLong(void)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		date.year = (uint16_t)(configdata[CONFIG_DATA_YEAR]+2000);
	}

	date.month = configdata[CONFIG_DATA_MONTH];
	date.dayOfMonth = configdata[CONFIG_DATA_DAY];
	date.hours = configdata[CONFIG_DATA_HOURS];
	date.minutes = configdata[CONFIG_DATA_MINUTES];
	date.seconds = configdata[CONFIG_DATA_SECONDS];
	date.day12 = 0;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		//die Echtzeituhr beitet die m�glichkeit die Tage mit zu z�hlen
		//wir berechnen hier 1x den Wochentag und lassen sie dann die Drecksarbeit machen
		//1=Sonntag, 7=Samstag
		date.dayOfWeek = dayOfWeek(date.dayOfMonth,date.month,date.year);
		
		sram_config.configIsSummertime = isSummertime();
		sram_config.configMagicValue1 = 0xDE;
		sram_config.configMagicValue2 = 0XAD;
		
		DS1307_writeToD(&date);
		DS1307_writeSRAMConfig_t();
		bDisplayOn = isDisplayOn(&date.hours,&date.minutes);
		bDisplayForcedOn = 0;
	}
	

	currMode = Clock;
	Setup_DeInitHandlers();

}

void Setup_KeyOK(void)
{
	if (subMode == MODE_SETUP_MAX )
	{
		subMode = Day;
	}
	else if (subMode == ColorMode)
	{
		if (sram_config.configColorMode == Fixed)
		{
			subMode = Color;
		}
		else
		{
			subMode ++;
		}		
	}
	else if (subMode == ColorTime)
	{
		subMode = SlotMachine;
	}
	else
	{
		subMode ++;
	}
}

//Setup Abbrechen
void Setup_KeyModeLong(void)
{
	currMode = Clock;
	sram_config = oldConfig;
	Setup_DeInitHandlers();
}

void Setup_KeyMode(void)
{
	
}

void Setup_KeyPlus(void)
{
	HandleModeSetup(Plus);
}

void Setup_KeyMinus(void)
{
	HandleModeSetup(Minus);
}

void Setup_On250msTick(void)
{
	if (bBlinkOn == true)
		bBlinkOn = false;
	else
		bBlinkOn = true;
}

void Setup_DoWork(void)
{
	HandleModeSetup(None);
}

void Setup_HourSwitched(void)
{
	//if (subMode == Hour || subMode == Minute || subMode == Second)
	//{
		checkBoundariesAndAdjust(Plus,&configdata[CONFIG_DATA_HOURS],59,0);
	//}
}

void Setup_MinuteSwitched(void)
{
	//if (subMode == Hour || subMode == Minute || subMode == Second)
	//{
		checkBoundariesAndAdjust(Plus,&configdata[CONFIG_DATA_MINUTES],59,0);
	//}
}

void Setup_SecondSwitched(void)
{
	//if (subMode == Hour || subMode == Minute || subMode == Second)
	//{
		if (bSkipAutoIncrement == false)
		{
			checkBoundariesAndAdjust(Plus,&configdata[CONFIG_DATA_SECONDS],59,0);
		}
		else
		{
			bSkipAutoIncrement = false;
		}
		
	//}
}

void Setup_DaySwitched(void)
{
	checkBoundariesAndAdjust(Plus,&configdata[CONFIG_DATA_DAY],31,0);
}

void Setup_MonthSwitched(void)
{
	checkBoundariesAndAdjust(Plus,&configdata[CONFIG_DATA_MONTH],12,0);
}

void Setup_YearSwitched(void)
{
	checkBoundariesAndAdjust(Plus,&configdata[CONFIG_DATA_YEAR],99,0);
}

void Setup_InitHandlers(void)
{
	EventHandler[HANDLER_OK_LONG] = Setup_KeyOKLong;
	EventHandler[HANDLER_OK] = Setup_KeyOK;
	EventHandler[HANDLER_PLUS] = Setup_KeyPlus;
	EventHandler[HANDLER_MINUS] = Setup_KeyMinus;
	EventHandler[HANDLER_250ms_TICK] = Setup_On250msTick;
	EventHandler[HANDLER_DOWORK] = Setup_DoWork;
	EventHandler[HANDLER_MODE_LONG] = Setup_KeyModeLong;
	EventHandler[HANDLER_SECOND_SWITCHED] = Setup_SecondSwitched;
	EventHandler[HANDLER_MINUTE_SWITCHED] = Setup_MinuteSwitched;
	EventHandler[HANDLER_HOUR_SWITCHED] = Setup_HourSwitched;
	EventHandler[HANDLER_DAY_SWITCHED] = Setup_DaySwitched;
	EventHandler[HANDLER_MONTH_SWITCHED] = Setup_MonthSwitched;
	EventHandler[HANDLER_YEAR_SWITCHED] = Setup_YearSwitched;
	bDisplayForcedOn = true; // Nixie-Netzteil und Hintergrundbeleuchtung auf jeden Fall einschalten Uhr k�nnte im Schlafmodus sein
	
	subMode = Init;
}

void Setup_DeInitHandlers(void)
{
	SetDefaultHandlers();
	bDisplayForcedOn = false;
	currMode = Clock;
	ModeInitHandlers[currMode]();
}


