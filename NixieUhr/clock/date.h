/*
 * clock.h
 *
 * Created: 24.07.2014 19:29:32
 *  Author: Sven Dummis
 */ 

#pragma once

void Date_InitHandlers(void);
void Date_DeInitHandlers(void);
void Date_KeyMode(void);
void Date_KeyModeLong(void);
void Date_DoWork(void);
void Date_On500msTick(void);
