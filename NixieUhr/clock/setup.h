/*
 * setup.h
 *
 * Created: 23.07.2014 17:57:11
 *  Author: Sven Dummis
 */ 

#pragma once
enum Setup_SubMode {
	Init = 0,
	Day = 1,
	Month = 2,
	Year = 3,
	Hour = 4,
	Minute = 5,
	Second = 6,
	SwitchOffHour = 7,
	SwitchOffMinute = 8,
	SwitchOnHour = 9,
	SwitchOnMinute = 10,
	ColorMode = 11,
	ColorTime = 12,
	Color = 13,
	SlotMachine = 14, 
	};

//#define MODE_SETUP_COLOR 7
#define MODE_SETUP_MAX 14

void Setup_InitHandlers(void);
void Setup_DeInitHandlers(void);
void Setup_KeyOKLong(void);
void Setup_KeyOK(void);
void Setup_KeyModeLong(void);
void Setup_KeyMode(void);
void Setup_KeyPlus(void);
void Setup_KeyMinus(void);
void Setup_On250msTick(void);
void Setup_DoWork(void);
void Setup_HourSwitched(void);
void Setup_MinuteSwitched(void);
void Setup_SecondSwitched(void);
void Setup_DaySwitched(void);
void Setup_MonthSwitched(void);
void Setup_YearSwitched(void);