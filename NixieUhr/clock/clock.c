/*
 * clock.c
 *
 * Created: 24.07.2014 19:28:51
 *  Author: Sven Dummis
 */ 

#include <avr/interrupt.h>
#include "../NixieUhr.h"
#include "../DS1307.h"
#include "../util.h"
#include "clock.h"

DS1307_ToD date;
uint8_t cnt;
uint8_t maxCnt;
bool bWakeUp;
void Clock_InitHandlers(void)
{
	EventHandler[HANDLER_MODE] = Clock_KeyMode;
	EventHandler[HANDLER_MODE_LONG] = Clock_KeyModeLong;
	EventHandler[HANDLER_DOWORK] = Clock_DoWork;
	EventHandler[HANDLER_500ms_TICK] = Clock_On500msTick;
	EventHandler[HANDLER_PLUS] = Clock_KeyPlus;
	EventHandler[HANDLER_OK] = Clock_KeyOk;
	cli();
	#ifndef DEBUG
	readConfig();
	#endif
	sei();
}
void Clock_KeyPlus(void)
{
	if (bDisplayOn == false)
	{
		bWakeUp = true;
		bDisplayForcedOn = true;
		cnt = 0;
		maxCnt = 20;
	}
}
void Clock_On500msTick(void)
{
	if (bWakeUp == true)
	{
		if (cnt < maxCnt)
		{
			cnt++;
			bDisplayForcedOn = true;
		}
		else
		{
			bDisplayForcedOn = false;
			bWakeUp = false;
		}
	}
}

void Clock_DoWork(void)
{
	shiftdata[SHIFT_DATA_HOURS] = bin2bcd(date.hours);
	shiftdata[SHIFT_DATA_MINUTES] = bin2bcd(date.minutes);
	shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(date.seconds);
	/*if (date.month == 10 && date.dayOfMonth == 4)
	{
		marriage = 1;
	}
	else
		marriage = 0;*/
		
	//ds1307_gettime_as_bcd(&shiftdata[SHIFT_DATA_HOURS], &shiftdata[SHIFT_DATA_MINUTES], &shiftdata[SHIFT_DATA_SECONDS]);
	shiftdata[SHIFT_DATA_DP] = 0; // f�r die dezimalpunkte
}


void Clock_KeyOk(void)
{
	if (bDisplayForcedOn == true)
	{
		bDisplayForcedOn = false;
	}
	else
	{
		bDisplayForcedOn = true;
	}
}

void Clock_KeyMode(void)
{
	checkBoundariesAndAdjust(Plus,&currMode,MAX_MODE,0);
	Clock_DeInitHandlers();
}

void Clock_KeyModeLong(void)
{
	currMode = Setup;
	Clock_DeInitHandlers();
}

void Clock_DeInitHandlers(void)
{
	SetDefaultHandlers();
	ModeInitHandlers[currMode]();
}