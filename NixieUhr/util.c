/*
 * util.c
 *
 * Created: 07.06.2014 18:29:53
 *  Author: Sven Dummis
 */ 
// Konvertiert BCD kodierte zahl in uint8_t
#include <stdint.h>
#include <avr/io.h>
#include <stdlib.h>
#include "util.h"
#include "NixieUhr.h"

bool isSummertime(void)
{
	if (date.month < 3 ||date.month > 10)
		return false;
	
	uint8_t wday = date.dayOfWeek - 1;
	
	// after last Sunday 2:00 // weekday: 0 = sunday 1 = monday
	if( (date.month - wday) >= 25 && (date.month || date.hours >= 2) )
	{
		if( date.month == 10 )		// October -> Winter
		return false;
	}
	else			// before last Sunday 2:00
	{
		if( date.month == 3 )				// March -> Winter
		return false;
	}

	return true;
}

bool isDisplayOn(uint8_t *hours, uint8_t *minutes)
{
	volatile bool bDspOn;
	
	bDspOn=false;
	// kleiner <
	// gr��er >
	
	// einschaltstunde ist gr��er als ausschalt stunde
	if (sram_config.configOnHour > sram_config.configOffHour)
	{
		// aktuelle stunde ist gr��er als die einschaltstunde jedoch kleiner als die ausschaltstunde
		if (*hours > sram_config.configOnHour && *hours < sram_config.configOffHour)
		{
			bDspOn = true;
		}
		else if (*hours == sram_config.configOffHour) //aktuelle stunde ist gleich der ausschaltstunde
		{
			if (*minutes <= sram_config.configOffMinute) //aktuelle minute ist kleiner oder gleich der ausschalt minute
			{
				bDspOn = true;
			}
		}
		else if(*hours == sram_config.configOnHour) //aktuelle stunde ist gleich der einschaltstunde
		{
			if (*minutes >= sram_config.configOnMinute) // aktuelle minute ist  gr��er oder gleich der einschalt minute
			{
				bDspOn = true;
			}
		}
	}
	else if (sram_config.configOnHour == sram_config.configOffHour) //einschaltstunde ist gleich ausschaltstunde
	{
		if (sram_config.configOnMinute > sram_config.configOffMinute) // einschaltminute ist gr��er als die ausschaltminute
		{
			if (*hours == sram_config.configOnHour) // aktuelle stunde = einschaltstunde/ausschaltstunde
			{
				//15-----------------35
				//aus -------------- ein
				if (!(*minutes >= sram_config.configOffMinute && *minutes <= sram_config.configOnMinute)) // aktuelle minute ist gr��er als die auschaltminute jedoch kleiner als die einschaltminute
				{
					bDspOn = true;
				}
			}
		}
		else if (sram_config.configOnMinute < sram_config.configOffMinute) //einschaltminute ist kleiner als die ausschaltminute
		{
			if (*minutes >= sram_config.configOnMinute && *minutes < sram_config.configOffMinute) // aktuelle minute ist gr��er oder gleich der einschalt minute jedoch kleiner als die ausschaltminute
			{
				bDspOn = true;
			}
		}
		if (sram_config.configOnMinute == sram_config.configOffMinute) //einschalt minute ist gleich ausschaltminute
		{
			bDspOn = true;
		}
	}
	else if (sram_config.configOnHour < sram_config.configOffHour) // einschaltstunde ist kleiner als ausschaltstunde
	{
		if (*hours > sram_config.configOnHour && *hours < sram_config.configOffHour)
		bDspOn = true;
		else if (*hours == sram_config.configOnHour)
		{
			if (*minutes >= sram_config.configOnMinute)
			{
				bDspOn = true;
			}
		}
		else if (*hours == sram_config.configOffHour)
		{
			if (*minutes < sram_config.configOffMinute)
			{
				bDspOn = true;
			}
		}
	}
	return bDspOn;
}
uint8_t bcd2bin (uint8_t val)
{
	return val - 6 * (val >> 4);
}

//konvertiert uint8_t in BCD kodierte zahl
uint8_t bin2bcd (uint8_t val)
{
	return val + 6 * (val / 10);
}
/*
	returns 1 on flipover otherwise 0
*/
uint8_t checkBoundariesAndAdjust(enum KeyAction Action, uint8_t* val, uint8_t upperBound, uint8_t lowerBound)
{
	uint8_t flipOver = 0;
	if (Action == Plus)
	{
		if (*val >= upperBound)
		{
			*val = lowerBound;
			flipOver = 1;
		}
		else
		{
			*val = *val + 1;
		}
	}
	else if (Action == Minus)
	{
		if (*val <= lowerBound)
		{
			*val = upperBound;
			flipOver = 1;
		}
		else
		{
			*val = *val - 1;
		}
	}
	return flipOver;
}

/*
H:          der Farbton als Farbwinkel H auf dem Farbkreis (z. B. 0� = Rot, 120� = Gr�n, 240� = Blau)
S:          die S�ttigung S in Prozent (z. B. 0% = keine Farbe, 50% = unges�ttigte Farbe, 100% = ges�ttigte, reine Farbe)
V:          der Grauwert V als Prozentwert angegeben (z. B. 0% = keine Helligkeit, 100% = volle Helligkeit)

Skalierung der HSV Werte:
H:      0-255, 0=rot, 42=gelb, 85=gr�n, 128=t�rkis, 171=blau, 214=violett
S:      0-255, 0=wei�t�ne, 255=volle Farben
V:      0-255, 0=aus, 255=maximale Helligkeit

*/
/*color_rgb_t hsv_to_rgb (color_hsv_t hsv)//unsigned char h, unsigned char s, unsigned char v, unsigned char channel)
{
	uint8_t r,g,b, i, f;
	unsigned int p, q, t;
	
	b=g=r=0;
	
	if( hsv.s == 0 )
	{  r = g = b = hsv.v;
	}
	else
	{  i=hsv.h/43;
		f=hsv.h%43;
		p = (hsv.v * (255 - hsv.s))/256;
		q = (hsv.v * ((10710 - (hsv.s * f))/42))/256;
		t = (hsv.v * ((10710 - (hsv.s * (42 - f)))/42))/256;

		switch( i )
		{  case 0:
			r = hsv.v; g = t; b = p; break;
			case 1:
			r = q; g = hsv.v; b = p; break;
			case 2:
			r = p; g = hsv.v; b = t; break;
			case 3:
			r = p; g = q; b = hsv.v; break;
			case 4:
			r = t; g = p; b = hsv.v; break;
			case 5:
			r = hsv.v; g = p; b = q; break;
		}
	}

	struct color_rgb color;
	color.r = r;
	color.g = g;
	color.b = b;

	return color;
}*/
color_rgb_t colorWheel(uint8_t wheelPos)
{
	color_rgb_t color;
	wheelPos = 255 - wheelPos;
	if(wheelPos < 85)
	{
		color.r = 255 - wheelPos * 3;
		color.g = 0;
		color.b = wheelPos * 3;
		return color;
	}
	else if(wheelPos < 170)
	{
		wheelPos -= 85;
		color.r = 0;
		color.g = wheelPos * 3;
		color.b  = 255 - wheelPos * 3;
		return color;
	}
	else
	{
		wheelPos -= 170;
		color.r = wheelPos * 3;
		color.g = 255 - wheelPos * 3;
		color.b = 0;
		return color;
	}
}
int DS1307_readSRAMConfig_t()
{
	uint8_t *p_sramConfig;
	p_sramConfig = (uint8_t*)&sram_config;
	return DS1307_readSRAM(p_sramConfig, sizeof(SRAMConfig_t));
}

int DS1307_writeSRAMConfig_t()
{
	uint8_t *p_sramConfig;
	p_sramConfig = (uint8_t*)&sram_config;
	return DS1307_writeSRAM(p_sramConfig,sizeof(SRAMConfig_t));
}

#define RANDOM_PORT PORTD
#define RANDOM_PIN PIND
#define RANDOM_DDR DDRD
#define RANDOM_BIT PD4

#define UPPPER_BOUND   59

int myRand()
{
	int x;

	while( (x = rand()) >= RAND_MAX - (RAND_MAX % UPPPER_BOUND) )
	;
	return x % UPPPER_BOUND;
}

uint16_t _fx_rand()
{
	static uint16_t lfsr = 0xACE1u;
	static uint16_t bit;
	bit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
	return lfsr =  (lfsr >> 1) | (bit << 15);
}
 
void fx_random(char *buf, uint16_t size){
	// set pin as input
	RANDOM_DDR &= ~_BV(RANDOM_BIT);
	RANDOM_PORT &= ~_BV(RANDOM_BIT);

	while(size){
		size--;
		buf[size] = 0;
		for(int c = 0; c < 8 * sizeof(char); c++){
			uint16_t timeout = 0xff;
			uint16_t x = 1;
			uint8_t d = RANDOM_PIN & _BV(RANDOM_BIT);
			while((d == (RANDOM_PIN & _BV(RANDOM_BIT))) && (timeout--)){
				x = _fx_rand();
			}
			buf[size] |= ((x & 1) << c);
		}
	}
}

