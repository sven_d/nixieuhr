/*
 * SlotMachine.h
 *
 * Created: 16.11.2014 18:31:51
 *  Author: Sven Dummis
 */ 


#ifndef SLOTMACHINE_H_
#define SLOTMACHINE_H_
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "ds1307.h"

#define REACHED_NOTHING		0
#define REACHED_HOUR		(1 << 0)  /* 000001 */
#define REACHED_MINUTE		(1 << 1)  /* 000010 */
#define REACHED_SECOND		(1 << 2)  /* 000100 */
#define REACHED_ALL			7

enum states {
	Disabled,
	Active,
	Started,
	ToZero,
	EnterIdleZero,
	IdleZero,
	LeaveIdleZero,
	ZeroReached,
	ToCycles,
	EnterIdleCycles,
	IdleCycles,
	LeaveIdleCycles,
	CyclesReached,
	ToTime,
	EnterIdleTime,
	IdleTime,
	LeaveIdleTime,
	TimeReached,
	Done,
};

typedef enum states MachineStates;

typedef struct {
	MachineStates mState;
	uint8_t cyclesToDo;
	uint8_t hourVal;
	uint8_t minuteVal;
	uint8_t secondVal;
	uint8_t cyclesDone;
	uint8_t skipCount;
	uint8_t skipsDone;
	DS1307_ToD* timedate;
		
} SlotMachineState_t;



SlotMachineState_t *SlotMachine_init(uint8_t cycles);

bool SlotMachine_isActive(SlotMachineState_t* machine_state);

bool SlotMachine_isDone(SlotMachineState_t* machine_state);

void SlotMachine_start(SlotMachineState_t* machine_state, DS1307_ToD* time_date);

void SlotMachine_doEffect(SlotMachineState_t* machine_state);

#endif /* SLOTMACHINE_H_ */