/*
 * debounce.h
 *
 * Created: 05.06.2014 09:42:44
 *  Author: Sven Dummis
 */ 
#pragma once

uint8_t get_key_press( uint8_t key_mask );

uint8_t get_key_rpt( uint8_t key_mask );

uint8_t get_key_short( uint8_t key_mask );

uint8_t get_key_long( uint8_t key_mask );
