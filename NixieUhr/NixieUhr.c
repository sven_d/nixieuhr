/*
 * NixieUhr.c
 *
 * Created: 05.06.2014 09:16:01
 *  Author: Sven Dummis
 */ 

//#include <stdlib.h>
//#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>


// f�r memcpy
//#include <string.h>

#ifndef NIXIEUHR_H_
#include "NixieUhr.h"
#endif

// alle Defines, die in den nachfolgenden Dateien gebraucht werden, sind in NixieUhr.h zu finden
#include "debounce.h"
#include "SoftPWM.h"
#include "ds1307.h"
//#include "DS1307.h"

#include "util.h"
#include "twilib.h"
#include "clock/setup.h"
#include "clock/clock.h"
#include "clock/date.h"
#include "SlotMachine.h"

#include <util/delay.h>

uint8_t ms500 = 0;
uint8_t ms250 = 0;
uint8_t ms50 = 0;

uint8_t bColorChanged = 0;

bool bIsWeddingDay = false;
bool bSlotMachineActive;

uint8_t eeBackColor[] EEMEM = {190,0,0}; // RGB-WERT der Hintergrundbeleuchtung

SlotMachineState_t *machineState;

#ifdef DEBUG
uint8_t cycleCount = 0;
#endif	

//build with (C#): (int)(Math.Pow((float)COLOR_VAL / (float)255, 2.8F) * 255 + 0.5)
//COLOR_VAL = 0 to 255
const uint8_t gammatable [] PROGMEM  = {
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
	2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
	5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
	10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
	17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
	25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
	37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
	51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
	69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
	90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
	115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
	144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
	177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
	215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };
	
uint8_t postDone = 0;



#ifdef PROTO
const uint8_t replaceTable [] PROGMEM = {1,0,9,8,7,6,5,4,3,2};

uint8_t replaceDigits(uint8_t value)
{
	uint8_t high;
	uint8_t low;
	high = HIGHNIBBLE(value);
	low = LOWNIBBLE(value);
	
	if (low != ILLEGAL_BCD)
		low = pgm_read_byte(replaceTable + low);
		
	if (high != ILLEGAL_BCD)
		high = pgm_read_byte(replaceTable + high);
		
	return COMBINENIBBLE(high,low);
}
#endif

void setupTimer1(void) //f�r SoftPWM
{
	// Timer 1 OCRA1, als variablen Timer nutzen

	TCCR1B = 2;             // Timer l�uft mit Prescaler 8
	TIMSK1 |= (1<<OCIE1A);   // Interrupt freischalten)
}

void setupTimer0(void)
{
	TCCR0B = (1<<CS02)|(1<<CS00);         // Vorteiler 1024
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		TCNT0 = (uint8_t)(int16_t)-(F_CPU / 1024 * 10e-3 + 0.5);  // Vorladung 10ms	
	}
	TIMSK0 |= 1<<TOIE0;
}

void setupIO(void)
{
	PWM_DDR = (1<<PORTC0) | (1<<PORTC1) | (1<<PORTC2);
	
	// PWM_DDR == DDRC !!!! siehe NixieUhr.h
	
	DDRC |= (1 << PORTC3);				 // ausgang zum einschalten des 170V Netzteils
	
	KEY_DDR &= ~ALL_KEYS;                // Key port f�r Input
	KEY_PORT |= ALL_KEYS;                // Pull-Ups aktivieren
}

void initSPI(void)
{
	//MAKE_OUT (PORT_MOSI);
	bit_set(DDRB, BIT(PORTB3));
	
	//MAKE_OUT (PORT_SCK);
	bit_set(DDRB, BIT(PORTB5));
	
	//MAKE_OUT (PORT_RCK); SET (PORT_RCK);
	bit_set(DDRB, BIT(PORTB2));
	bit_set(PORTB, BIT(PORTB2));

	// !!! SS muss OUT sein, damit SPI nicht in Slave-Mode wechselt !!!
	// entfaellt, falls PORT_RCK = PORT_SS


	// SPI als Master
	// High-Bits zuerst
	// SCK ist HIGH wenn inaktiv
	SPCR = (1 << SPE) | (1 << MSTR) | (1 << CPOL);
	
	// pullup an MISO vermeidet Floaten
	//TODO: CHECK
	//SET (PORT_MISO);

	// maximale Geschwindigkeit: F_CPU / 2
	SPSR |= (1 << SPI2X);
	SPCR |= (1 << SPR1) | (1 << SPR0);
	
}

void DPOn(void)
{
	//    -- HH	MM SS
	// 0b 00 11 11 11
	//       lr lr lr
	// 0b 00 01 01 00

	shiftdata[SHIFT_DATA_DP] = (1<<DP_RIGHT_HOUR) | (1<<DP_RIGHT_MINUTE);
}

void DPOff(void)
{
	shiftdata[SHIFT_DATA_DP] = 0;
}



void tickms500(void)
{
	static uint8_t iCnt = 0;
	
	if (iCnt == 50)
	{
		ms500 = 1;
		iCnt=0;
	}
	else
	{
		ms500 = 0;
		iCnt++;
	}
}

void tickms250(void)
{
	static uint8_t iCnt = 0;
	
	if (iCnt == 25)
	{
		ms250 = 1;
		iCnt=0;
	}
	else
	{
		ms250 = 0;
		iCnt++;
	}
}

void tickms50(void)
{
	static uint8_t iCnt = 0;
	
	if (iCnt == 5)
	{
		ms50 = 1;
		iCnt=0;
	}
	else
	{
		ms50 = 0;
		iCnt++;
	}
}



void OnKeyMode(void)
{
	
}

void OnKeyModeLong(void)
{

}

void OnKeyPlus(void)
{

}

void OnKeyMinus(void)
{

}

void OnKeyOk(void)
{
	
}

void OnKeyOkLong(void)
{
	
}

void On250msTick(void)
{
	
}

void On500msTick(void)
{
	
}

void OnSecondSwitched(void)
{
	
}

void OnMinuteSwitched(void)
{
	
}

void OnHourSwitched(void)
{
	
}

void OnDaySwitched(void)
{
	
}

void OnMonthSwitched(void)
{
	
}

void OnYearSwitched(void)
{
	
}

void DoWork(void)
{
	
}

void OnTimer0Tick(void)
{
	tickms50();
	tickms250();
	tickms500();
}

void shift(void)
{
	#ifdef PROTO // work around f�r prototyp 
	uint8_t i;
	for (i=1; i < 4; i++)
	{
		shiftdata[i] = replaceDigits(shiftdata[i]);
	}
	#endif
			
	unsigned char anz = SHIFT_DATA_SIZE;
	unsigned char* shift_data = shiftdata+SHIFT_DATA_SIZE;
	
	do
	{
		unsigned char data = *--shift_data;

		// SPDR schreiben startet Uebertragung
		SPDR = data;
#ifndef DEBUG
		// warten auf Ende der Uebertragung f�r dieses Byte
		while (!(SPSR & (1 << SPIF)));
#endif
		// clear SPIF durch Lesen von SPDR
		(void) SPDR;
	}
	while (--anz > 0);

	// Strobe an RCK bringt die Daten von den Schieberegistern in die Latches
	//CLR (PORT_RCK);
	//SET (PORT_RCK);
	bit_clear(PORTB, BIT(PORTB2));
	bit_set(PORTB, BIT(PORTB2));
	
}

void readConfig(void)
{
	eeprom_read_block (backColor, eeBackColor, sizeof(backColor));
	DS1307_readSRAMConfig_t();
}

EventHandler_t GetDefaultHandler(uint8_t handler)
{
	EventHandler_t eventHandler = 0;
	switch (handler)
	{
		case HANDLER_MODE:
			eventHandler = OnKeyMode;
			break;
		case HANDLER_MODE_LONG:
			eventHandler = OnKeyModeLong;
			break;
		case HANDLER_OK:
			eventHandler = OnKeyOk;
			break;
		case HANDLER_OK_LONG:
			eventHandler = OnKeyOkLong;
			break;
		case HANDLER_PLUS:
			eventHandler = OnKeyPlus;
			break;
		case HANDLER_MINUS:
			eventHandler = OnKeyMinus;
			break;
		case HANDLER_DOWORK:
			eventHandler = DoWork;
			break;
		case HANDLER_250ms_TICK:
			eventHandler = On250msTick;
			break;
		case HANDLER_500ms_TICK:
			eventHandler = On500msTick;
			break;
		case HANDLER_HOUR_SWITCHED:
			eventHandler = OnHourSwitched;
			break;
		case HANDLER_MINUTE_SWITCHED:
			eventHandler = OnMinuteSwitched;
			break;		
		case HANDLER_SECOND_SWITCHED:
			eventHandler = OnSecondSwitched;
			break;
		case HANDLER_YEAR_SWITCHED:
			eventHandler = OnYearSwitched;
			break;
		case HANDLER_MONTH_SWITCHED:
			eventHandler = OnMonthSwitched;
			break;
		case HANDLER_DAY_SWITCHED:
			eventHandler = OnDaySwitched;
			break;
	}
	
	return eventHandler;
}

void SetHandler(uint8_t handler, EventHandler_t callBack)
{
	EventHandler[handler] = callBack;
}

void SetDefaultHandlers()
{
	SetHandler(HANDLER_MODE,GetDefaultHandler(HANDLER_MODE));
	SetHandler(HANDLER_MODE_LONG,GetDefaultHandler(HANDLER_MODE_LONG));
	SetHandler(HANDLER_OK,GetDefaultHandler(HANDLER_OK));
	SetHandler(HANDLER_OK_LONG,GetDefaultHandler(HANDLER_OK_LONG));
	SetHandler(HANDLER_PLUS,GetDefaultHandler(HANDLER_PLUS));
	SetHandler(HANDLER_MINUS,GetDefaultHandler(HANDLER_MINUS));
	SetHandler(HANDLER_DOWORK,GetDefaultHandler(HANDLER_DOWORK));
	SetHandler(HANDLER_250ms_TICK,GetDefaultHandler(HANDLER_250ms_TICK));
	SetHandler(HANDLER_500ms_TICK,GetDefaultHandler(HANDLER_500ms_TICK));
	SetHandler(HANDLER_SECOND_SWITCHED,GetDefaultHandler(HANDLER_SECOND_SWITCHED));
	SetHandler(HANDLER_MINUTE_SWITCHED,GetDefaultHandler(HANDLER_MINUTE_SWITCHED));
	SetHandler(HANDLER_HOUR_SWITCHED,GetDefaultHandler(HANDLER_HOUR_SWITCHED));
	SetHandler(HANDLER_DAY_SWITCHED,GetDefaultHandler(HANDLER_DAY_SWITCHED));
	SetHandler(HANDLER_MONTH_SWITCHED,GetDefaultHandler(HANDLER_MONTH_SWITCHED));
	SetHandler(HANDLER_YEAR_SWITCHED,GetDefaultHandler(HANDLER_YEAR_SWITCHED));
}

void checkAndDoTimeShift(void)
{
	// ist m�rz oder oktober?
	if ((date.month == 3 || date.month == 10))
	{
		// der Sonntag kann nur auf den 31 oder 7 Tage fr�her fallen
		if ((date.dayOfMonth >= 25) && (date.dayOfMonth <= 31))
		{
			// ist heute Sonntag?
			if (date.dayOfWeek == 1)
			{
				//ist es m�rz um 2?
				if ((date.month == 3) && (date.hours == 2))
				{
					date.hours = 3;
					sram_config.configIsSummertime = 1;
					ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
					{
						DS1307_writeToD(&date);
						DS1307_writeSRAMConfig_t();
					}
					
				} //oder oktober um 3?
				else if ((date.month == 10) && (date.hours == 3) && (sram_config.configIsSummertime == 1))
				{
					date.hours = 2;
					sram_config.configIsSummertime = 0;
					ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
					{
						DS1307_writeToD(&date);
						DS1307_writeSRAMConfig_t();
					}
				}
			}
		}
	}
}

void doWheelColor(void)
{
	static uint8_t colorWaitCycles = 0;
	if (sram_config.configColorMode == Wheel)
	{
		if (colorWaitCycles != sram_config.configColorTime)
		{
			colorWaitCycles ++;
		}
		else
		{
			colorWaitCycles = 0;
			sram_config.colorIndex ++;
		}
	}
}

#ifdef DEBUG
void simulateRTC(void)
{
	if (cycleCount == 3)
	{
		cycleCount = 0;
		if (checkBoundariesAndAdjust(Plus,&date.seconds,59,0))
		{
			if (checkBoundariesAndAdjust(Plus,&date.minutes,59,0))
			{
				if (checkBoundariesAndAdjust(Plus,&date.hours,23,0))
				{
					if (checkBoundariesAndAdjust(Plus,&date.dayOfMonth,31,1))
					{
						checkBoundariesAndAdjust(Plus,&date.month,12,1);
					}
				}
			}
		}
	}
	else
	{
		cycleCount ++;
	}
}

void setTestValues(void)
{
	date.minutes = 6;
	date.seconds = 15;
	date.hours = 10;
	date.year = 2015;
	date.month = 8;
	date.dayOfMonth = 11;
	date.dayOfWeek = 2;
	sram_config.colorR = 1;
	sram_config.colorG = 2;
	sram_config.colorB = 3;
	sram_config.configOnHour = 17;
	sram_config.configOnMinute = 30;
	sram_config.configOffHour = 23;
	sram_config.configOffMinute = 00;
	sram_config.configIsSummertime = 1;
	sram_config.configSlotMachine = 1;
	sram_config.configMagicValue1 = 0xDE;
	sram_config.configMagicValue2 = 0xAD;
}
#endif



void prepareDisplay(void)
{
	if (bDisplayOn == true || bDisplayForcedOn == true)
	{
		if (sram_config.configColorMode == Wheel)
		{
			color_rgb_t color = colorWheel(sram_config.colorIndex);
			pwm_setting[0]=pgm_read_byte(&gammatable[color.r]);
			pwm_setting[1]=pgm_read_byte(&gammatable[color.g]);
			pwm_setting[2]=pgm_read_byte(&gammatable[color.b]);
		}
		else
		{
			pwm_setting[0]=pgm_read_byte(&gammatable[sram_config.colorR]);
			pwm_setting[1]=pgm_read_byte(&gammatable[sram_config.colorG]);
			pwm_setting[2]=pgm_read_byte(&gammatable[sram_config.colorB]);
		}
		
		if (bIsWeddingDay == true)
		{			
			pwm_setting[0]=pgm_read_byte(&gammatable[100]);
			pwm_setting[1]=pgm_read_byte(&gammatable[0]);
			pwm_setting[2]=pgm_read_byte(&gammatable[60]);			
		}

		PORTC |= (1 << PORTC3);
	}
	else
	{
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			pwm_setting[0] = 0;
			pwm_setting[1] = 0;
			pwm_setting[2] = 0;
			PORTC &= ~(1 << PORTC3);
		}
	}
}
int main(void)
{

	uint16_t lastYear = 0;
	uint8_t lastMonth = 0;
	uint8_t lastDay = 0;
	uint8_t lastHour = 0;
	uint8_t lastMinute = 0;
	uint8_t lastSecond = 0;

	bool bHourSwitched = true;
	bool bStartUp = true;
	
	int seed;
	seed = 0;
	fx_random((char*)&seed, sizeof(int)); // Aufruf mu� vor setupIO erfolgen, da wir sonst ins gehege mit der debounce-Funktion kommen
	
	setupIO();
	
	setupTimer0(); //Taster entprellen / evtl... system-tick Interrupt 10ms
	
	setupTimer1(); // SoftPWM
	
	initSPI();	// SPI initialisieren, daran h�ngen die schieberegister
	
	#ifndef DEBUG
	_delay_ms(500);
	#endif
	PORTC |= (1 << PORTC3);  // Nixie-Netzteil einschalten
	#ifndef DEBUG
	_delay_ms(500);
	#endif
	
	shiftdata[0] = 0;
	shiftdata[1] = 0;
	shiftdata[2] = 0;
	shiftdata[3] = 0;
	shift();
#ifndef DEBUG
	_delay_ms(1000);
	I2C_Config *masterConfig = I2C_buildDefaultConfig();
	shiftdata[0] = 0;
	shiftdata[SHIFT_DATA_HOURS] = 0;
	shiftdata[SHIFT_DATA_MINUTES] = 0;
	shiftdata[SHIFT_DATA_SECONDS] = 1;
	shift();
	I2C_masterBegin(masterConfig);
	_delay_ms(1000);
	shiftdata[0] = 0;
	shiftdata[SHIFT_DATA_HOURS] = 0;
	shiftdata[SHIFT_DATA_MINUTES] = 0;
	shiftdata[SHIFT_DATA_SECONDS] = 2;
	shift();
	_delay_ms(1000);
	DS1307_readToD(&date);
	shiftdata[0] = 0;
	shiftdata[SHIFT_DATA_HOURS] = 0;
	shiftdata[SHIFT_DATA_MINUTES] = 0;
	shiftdata[SHIFT_DATA_SECONDS] = 3;
	shift();
	_delay_ms(1000);
	
	
	srand(seed);
	uint8_t rndMinutes;
	rndMinutes = (uint8_t)myRand();
	uint8_t rndSeconds;
	rndSeconds = (uint8_t)myRand();
	shiftdata[SHIFT_DATA_HOURS] = 0;
	shiftdata[SHIFT_DATA_MINUTES] = 0;
	shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(rndMinutes);
	shift();
	
	_delay_ms(1000);
	
	shiftdata[SHIFT_DATA_HOURS] = 0;
	shiftdata[SHIFT_DATA_MINUTES] = 0;
	shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(rndSeconds);
	shift();
#else
	setTestValues();
#endif
#ifndef DEBUG
	_delay_ms(1000);
	_delay_us(10);
	
	readConfig();
#endif
	
	SetDefaultHandlers();
	
	ModeInitHandlers[Clock] = Clock_InitHandlers;
	ModeInitHandlers[Date] = Date_InitHandlers;
	ModeInitHandlers[Setup] = Setup_InitHandlers;

	bColorChanged = 1;
		
	//Batterie scheint tot gewesen zu sein oder wir starten das 1. mal
	// alle werte auf plausible defaults setzen und setup starten
	if ((sram_config.configMagicValue1 != 0xDE) || (sram_config.configMagicValue2 != 0xAD))
	{
		date.minutes = 0;
		date.seconds = 0;
		date.hours = 0;
		date.year = 2014;
		date.month = 1;
		date.dayOfMonth = 1;
		date.dayOfWeek = 1;
		sram_config.colorR = 0;
		sram_config.colorG = 0;
		sram_config.colorB = 0;
		sram_config.configOnHour = 0;
		sram_config.configOnMinute = 0;
		sram_config.configOffHour = 0;
		sram_config.configOffMinute = 0;
		sram_config.configIsSummertime = 0;
		sram_config.configColorMode = 0;
		sram_config.configColorTime = 0;
		sram_config.colorIndex = 0;
		sram_config.configSlotMachine = 0;
		ModeInitHandlers[Setup]();
	}
	else
	{
		ModeInitHandlers[Clock]();
	}
	
	bDisplayOn = isDisplayOn(&date.hours, &date.minutes);
	
	if (date.month == 10 && date.dayOfMonth == 4)
		bIsWeddingDay = true;
	else
		bIsWeddingDay = false;
	
	machineState = SlotMachine_init(25);
	while(1)
    {
		if (cnt_10ms == 1)
		{
			OnTimer0Tick();
			doWheelColor();
			
			if (bSlotMachineActive == true)
			{
				SlotMachine_doEffect(machineState);
			}
				
			cnt_10ms = 0;		
		}
		
		#ifdef DEBUG
			ms250=1;
		#endif
		if (ms50 == 1)
		{
			#ifndef DEBUG
			DS1307_readToD(&date);
			#endif
		}
		
		if (ms250 == 1)
		{
			EventHandler[HANDLER_250ms_TICK]();		
		}
		
		if (ms500 == 1)
		{
			EventHandler[HANDLER_500ms_TICK]();
			#ifdef DEBUG
			simulateRTC();
			#endif
		}
		
		if (lastSecond != date.seconds)
		{
			lastSecond = date.seconds;
			EventHandler[HANDLER_SECOND_SWITCHED]();
			
			if (sram_config.configSlotMachine == 1)
			{
				
				if (date.minutes == rndMinutes && date.seconds == rndSeconds && bHourSwitched == true)
				{
					SlotMachine_start(machineState, &date);
					rndMinutes = (uint8_t)myRand();
					rndSeconds = (uint8_t)myRand();					
					bSlotMachineActive = true;
				}
				
				if (bStartUp == true)
				{
					bStartUp = false;
					bHourSwitched = true;
					SlotMachine_start(machineState, &date);
					bSlotMachineActive = true;
				}
				
			}
		}
		
		if (lastMinute != date.minutes)
		{
			lastMinute = date.minutes;
			// es k�nnte sein das wir auf displayOn zur�ckgreifen desshalb
			// status desshalb vor event-call ermitteln
			bDisplayOn = isDisplayOn(&date.hours, &date.minutes);
			
			// status des erzwungnen einschalten l�schen wenn display regul�r eingeschalten wird
			if (bDisplayForcedOn == true && bDisplayOn == true)
			{
				bDisplayForcedOn = false;
			}
			
			EventHandler[HANDLER_MINUTE_SWITCHED]();
			
		}
		
		if (lastHour != date.hours)
		{
			lastHour = date.hours;
			checkAndDoTimeShift();
			bHourSwitched = true;
			EventHandler[HANDLER_HOUR_SWITCHED]();
		}
		
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			if (lastYear != date.year)
			{
				lastYear = date.year;
				EventHandler[HANDLER_YEAR_SWITCHED]();
			}
		}
		
		
		if (lastMonth != date.month)
		{
			lastMonth = date.month;				
			EventHandler[HANDLER_MONTH_SWITCHED]();
		}
		
		if (lastDay != date.dayOfMonth)
		{
			lastDay = date.dayOfMonth;
			
			if (date.month == 10 && date.dayOfMonth == 4)
				bIsWeddingDay = true;
			else
				bIsWeddingDay = false;
			
			EventHandler[HANDLER_DAY_SWITCHED]();
		}
		
		if (get_key_short(1<<KEY_MODE))
		{
			EventHandler[HANDLER_MODE]();
		}
		
		if (get_key_long(1<<KEY_MODE))
		{
			EventHandler[HANDLER_MODE_LONG]();
		}
		
		if (get_key_press(1<<KEY_PLUS) || get_key_rpt(1<<KEY_PLUS))
		{
			EventHandler[HANDLER_PLUS]();
		}
		
		if (get_key_press(1<<KEY_MINUS) || get_key_rpt(1<<KEY_MINUS))
		{
			EventHandler[HANDLER_MINUS]();
		}
		
		if (get_key_short(1<<KEY_OK))
		{
			EventHandler[HANDLER_OK]();
		}
		
		if (get_key_long(1<<KEY_OK))
		{
			EventHandler[HANDLER_OK_LONG]();
		}

		EventHandler[HANDLER_DOWORK]();
		
		if (bSlotMachineActive == true)
		{
			shiftdata[SHIFT_DATA_HOURS] = bin2bcd(machineState->hourVal);
			shiftdata[SHIFT_DATA_MINUTES] = bin2bcd(machineState->minuteVal);
			shiftdata[SHIFT_DATA_SECONDS] = bin2bcd(machineState->secondVal);
			
			if (SlotMachine_isDone(machineState) == true)
				bSlotMachineActive = false;
		}
		
		prepareDisplay();
		
		shift();
		pwm_update();
#ifdef DEBUG
		cnt_10ms = 1;
#endif
	
		ms50 = 0;
		ms250 = 0;
		ms500 = 0;
    }
}