/*
 * SoftPWM.h
 *
 * Created: 05.06.2014 09:30:30
 *  Author: Sven Dummis
 */ 
#pragma once

#include "NixieUhr.h"


void pwm_update(void);

uint8_t  pwm_setting[PWM_CHANNELS];

