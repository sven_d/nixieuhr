/*
 * NixieUhr.h
 *
 * Created: 05.06.2014 09:23:02
 *  Author: Sven Dummis
 */ 
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "ds1307.h"
#define PROTO // Prototyp, fehler im NIXIE Pinout

#ifndef NIXIEUHR_H_
#define NIXIEUHR_H_

#define F_CPU         8000000L           // Systemtakt in Hz

#define F_PWM         100L               // PWM-Frequenz in Hz
#define PWM_PRESCALER 8                  // Vorteiler für den Timer
#define PWM_STEPS     256                // PWM-Schritte pro Zyklus(1..256)
#define PWM_PORT      PORTC              // Port für PWM
#define PWM_DDR       DDRC               // Datenrichtungsregister für PWM
#define PWM_CHANNELS  3                  // Anzahl der PWM-Kanäle

#define KEY_DDR         DDRD
#define KEY_PORT        PORTD
#define KEY_PIN         PIND
#define KEY0            0
#define KEY1            1
#define KEY2            2
#define KEY3			3
#define KEY4			4
#define KEY_MODE		4 //ALIAS
#define KEY5			5
#define KEY_PLUS		5 //ALIAS
#define KEY6			6
#define KEY_MINUS		6 //ALIAS
#define KEY7			7
#define KEY_OK			7 //ALIAS
#define ALL_KEYS        (1<<KEY4 | 1<<KEY5 | 1<<KEY6 | 1<<KEY7)

#define HANDLER_MODE			0
#define HANDLER_MODE_LONG		1
#define HANDLER_OK				2
#define HANDLER_OK_LONG			3
#define HANDLER_PLUS			4
#define HANDLER_MINUS			5
#define HANDLER_DOWORK			6
#define HANDLER_250ms_TICK		7
#define HANDLER_500ms_TICK		8
#define HANDLER_SECOND_SWITCHED	9
#define HANDLER_MINUTE_SWITCHED	10
#define HANDLER_HOUR_SWITCHED	11
#define HANDLER_DAY_SWITCHED	12
#define HANDLER_MONTH_SWITCHED	13
#define HANDLER_YEAR_SWITCHED	14

typedef void (*EventHandler_t)(void);
EventHandler_t EventHandler[15];
EventHandler_t GetDefaultHandler(uint8_t handler);
void SetDefaultHandlers();

enum KeyAction {
	None = 0,
	Plus = 1,
	Minus = 2,
	Okay = 3,
	};
/*#define KEYACTION_NONE		0
#define KEYACTION_PLUS		1
#define KEYACTION_MINUS		2
#define KEYACTION_OKAY		3
#define KEYACTION_OKAY_LONG 4*/

#define REPEAT_MASK     (1<<KEY4 | 1<<KEY5 | 1<<KEY6 | 1<<KEY7)       // repeat: key5, key6
#define REPEAT_START    50                        // after 500ms
#define REPEAT_NEXT     20						  // every 200ms


enum Mode {
	Clock = 0,
	Date = 1,
	Setup = 2,
	};
/*#define MODE_CLOCK 0
#define MODE_DATECLOCK 1*/
#define MAX_MODE 1
/*#define MODE_SETUP MAX_MODE+1*/ // spezieller Fall soll nie über kurzen Tastendruck des Mode-Tasters wählbar sein

EventHandler_t ModeInitHandlers[Setup+1];

#define DP_RIGHT_SECOND 0
#define DP_LEFT_SECOND 1
#define DP_RIGHT_MINUTE 2
#define DP_LEFT_MINUTE 3
#define DP_RIGHT_HOUR 4
#define DP_LEFT_HOUR 5
//TODO: Reihenfolge anpassen
#define SHIFT_DATA_HOURS 3
#define SHIFT_DATA_MINUTES 2
#define SHIFT_DATA_SECONDS 1

#define SHIFT_DATA_DAY 3
#define SHIFT_DATA_MONTH 2
#define SHIFT_DATA_YEAR 1

//TODO: SHIFT_DATA_SIZE anpassen
#define SHIFT_DATA_DP 0
#define SHIFT_DATA_SIZE 4

#define ILLEGAL_BCD 15

#define CONFIG_DATA_HOURS 0
#define CONFIG_DATA_MINUTES 1
#define CONFIG_DATA_SECONDS 2
#define CONFIG_DATA_DAY 3
#define CONFIG_DATA_MONTH 4
#define CONFIG_DATA_YEAR 5

typedef struct {
	uint8_t colorIndex;
	uint8_t colorR;
	uint8_t colorG;
	uint8_t colorB;
	uint8_t configOnHour;
	uint8_t configOnMinute;
	uint8_t	configOffHour;
	uint8_t	configOffMinute;
	uint8_t	configIsSummertime;
	uint8_t configColorMode;
	uint8_t configColorTime;
	uint8_t configMagicValue1;
	uint8_t configMagicValue2;
	uint8_t configSlotMachine;
	} SRAMConfig_t;

enum Color_Mode {
	Fixed = 0,
	Wheel = 1,
	};
/*enum SRAM_ConfigKeys {
	colorIndex = 0,
	colorR = 1,
	colorG = 2,
	colorB = 3,
	configOnHour = 4,
	configOnMinute = 5,
	configOffHour = 6,
	configOffMinute = 7,
	configIsSummertime = 8,
	};*/
	
//#define SRAM_CONFIG_SIZE 9

SRAMConfig_t sram_config;
	
volatile uint8_t cnt_10ms;

uint8_t shiftdata[SHIFT_DATA_SIZE];

uint8_t configdata[6];

enum Mode currMode;

uint8_t backColor[3];

bool bDisplayOn;
bool bDisplayForcedOn;

DS1307_ToD date;

void readConfig(void);
//uint8_t subMode;


#endif /* NIXIEUHR_H_ */