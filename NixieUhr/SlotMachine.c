/*
 * SlotMachine.c
 *
 * Created: 16.11.2014 18:31:37
 *  Author: Sven Dummis
 */ 
#include "SlotMachine.h"
#include "util.h"
#include "ds1307.h"
#include <stdbool.h>

SlotMachineState_t state;

uint8_t incrementNibbles(uint8_t Val)
{
	uint8_t lowNibble;
	uint8_t highNibble;
	
	Val = bin2bcd(Val);
	lowNibble = LOWNIBBLE(Val);
	highNibble = HIGHNIBBLE(Val);
	if (lowNibble == 9)
	{
		lowNibble = 0;
	}
	else
	{
		lowNibble++;
	}
	if (highNibble == 9)
	{
		highNibble = 0;
	}
	else
	{
		highNibble++;
	}
	
	return bcd2bin(COMBINENIBBLE(highNibble,lowNibble));
}

// cycles definiert wie oft die Ziffern durchlaufen sollen
SlotMachineState_t *SlotMachine_init(uint8_t cycles)
{
	state.mState = Disabled;
	state.hourVal = 0;
	state.minuteVal = 0;
	state.secondVal = 0;
	state.cyclesToDo = cycles;
	
	return &state;
}

void SlotMachine_start(SlotMachineState_t* machine_state, DS1307_ToD* time_date)
{
	machine_state->mState = Active;
	machine_state->hourVal = time_date->hours;
	machine_state->minuteVal = time_date->minutes;
	machine_state->secondVal = time_date->seconds;
	machine_state->timedate = time_date;
	machine_state->cyclesDone = 0;
	machine_state->skipCount = 15;
	machine_state->skipsDone = 0;
}

uint8_t checkAndIncrement(uint8_t desired, uint8_t actual)
{
	if (actual != desired)
	{
		if (actual == 9)
		{
			actual = 0;
		}
		else
		{
			actual++;
		}
	}
	return actual;
}

void check(uint8_t* desired, uint8_t* actual)
{
	uint8_t lowNibbleDesired;
	uint8_t highNibbleDesired;
	uint8_t lowNibbleActual;
	uint8_t highNibbleActual;
	
	*actual = bin2bcd(*actual);
	
	lowNibbleDesired = LOWNIBBLE(bin2bcd(*desired));
	highNibbleDesired = HIGHNIBBLE(bin2bcd(*desired));
	lowNibbleActual = LOWNIBBLE(*actual);
	highNibbleActual = HIGHNIBBLE(*actual);
	
	lowNibbleActual=checkAndIncrement(lowNibbleDesired,lowNibbleActual);
	highNibbleActual=checkAndIncrement(highNibbleDesired,highNibbleActual);
	*actual = bcd2bin(COMBINENIBBLE(highNibbleActual,lowNibbleActual));
}

bool SlotMachine_RunToZero(SlotMachineState_t* machine_state)
{
	uint8_t zero;
	zero = 0;
	
	if (machine_state->hourVal != 0)
	{
		check(&zero,&machine_state->hourVal);
	}
	if (machine_state->minuteVal != 0)
	{
		check(&zero,&machine_state->minuteVal);
	}
	if (machine_state->secondVal != 0)
	{
		check(&zero,&machine_state->secondVal);
	}
	
	if( machine_state->hourVal == 0 && machine_state->minuteVal == 0 && machine_state->secondVal == 0)
		return true;
	else
		return false;
}

bool SlotMachine_doIdle(SlotMachineState_t* machine_state)
{
	if (machine_state->skipsDone < (machine_state->skipCount -1))
	{
		machine_state->skipsDone = machine_state->skipsDone + 1;
		return false;
	}
	else
	{
		return true;
	}
}

bool SlotMachine_isDone(SlotMachineState_t* machine_state)
{
	if (machine_state->mState == Done)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool SlotMachine_IsActive(SlotMachineState_t* machine_state)
{
	if (machine_state->mState != Done || machine_state->mState != Disabled)
	{
		return true;
	}
	else
	{
		return false;
	}
}
// mu� alle 10ms aufgerufen werden (system tick)
void SlotMachine_doEffect(SlotMachineState_t* machine_state)
{
	
	uint8_t reached;
	
	reached = REACHED_NOTHING;
	
	switch (machine_state->mState)
	{
		case Active:
			machine_state->mState = Started;
			break;
			
		case Started:
			machine_state->mState = ToZero;
			break;
			
		case ToZero:
			if (SlotMachine_RunToZero(machine_state))
			{
				machine_state->mState = ZeroReached;
			}
			else
			{
				machine_state->mState = EnterIdleZero;
			}
			break;
			
		case EnterIdleZero:
			machine_state->skipsDone = 0;
			machine_state->mState = IdleZero;
			break;
			
		case IdleZero:
			if (SlotMachine_doIdle(machine_state))
			{
				machine_state->mState = LeaveIdleZero;
			}
			break;
			
		case LeaveIdleZero:
			machine_state->mState = ToZero;
			break;
			
		case ZeroReached:
			machine_state->mState = ToCycles;
			break;
			
		case ToCycles:
			if (machine_state->cyclesDone == machine_state->cyclesToDo)
			{
				machine_state->mState = CyclesReached;
			}
			else
			{
				machine_state->secondVal = incrementNibbles(machine_state->secondVal);
				machine_state->minuteVal = incrementNibbles(machine_state->minuteVal);
				machine_state->hourVal = incrementNibbles(machine_state->hourVal);
				machine_state->cyclesDone = machine_state->cyclesDone + 1;
				machine_state->mState = EnterIdleCycles;
			}
			break;
			
		case EnterIdleCycles:
			machine_state->skipsDone = 0;
			machine_state->mState = IdleCycles;
			break;
			
		case IdleCycles:
			if (SlotMachine_doIdle(machine_state))
			{
				machine_state->mState = LeaveIdleCycles;
			}
			break;
			
		case LeaveIdleCycles:
			machine_state->mState = ToCycles;
			break;
				
		case CyclesReached:
			machine_state->mState = ToTime;
			break;
		
		case ToTime:
		
			if (machine_state->timedate->hours != machine_state->hourVal)
			{
				check(&machine_state->timedate->hours,&machine_state->hourVal);
			}
			else
			{
				reached |= REACHED_HOUR;
			}
			
			if (machine_state->timedate->minutes != machine_state->minuteVal)
			{
				check(&machine_state->timedate->minutes,&machine_state->minuteVal);
			}
			else
			{
				reached |= REACHED_MINUTE;
			}
			
			if(machine_state->timedate->seconds != machine_state->secondVal)
			{
				check(&machine_state->timedate->seconds,&machine_state->secondVal);
			}
			else
			{
				reached |= REACHED_SECOND;
			}
			
			if (reached == REACHED_ALL)
				machine_state->mState = TimeReached;
			else
				machine_state->mState = EnterIdleTime;
				
			break;
		case EnterIdleTime:
			machine_state->skipsDone = 0;
			machine_state->mState = IdleTime;
			break;
			
		case IdleTime:
			if (SlotMachine_doIdle(machine_state))
			{
				machine_state->mState = LeaveIdleTime;
			}
			break;
			
		case LeaveIdleTime:
			machine_state->mState = ToTime;
			break;
			
		case TimeReached:
			machine_state->mState = Done;
			break;
			
		case Done:
			return;
			
		case Disabled:
			return;
		
	}
}